
install: venv
	venv/bin/pip install --upgrade ansible
	if command -v ansible; then :; else \
		sudo ln -s ${PWD}/venv/bin/ansible /usr/local/bin; fi

venv:
	python3 -m ensurepip --user
	python3 -m venv venv
